<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="outside - tf" tilewidth="16" tileheight="16" tilecount="1568" columns="56">
 <image source="../Art/TimeFantasy_RPG_Tiles/outside.png" width="896" height="448"/>
 <tile id="272">
  <objectgroup draworder="index">
   <object id="1" x="1" y="7.8125">
    <polygon points="15.0625,5 10.4375,6.0625 3,4.5 0,0 6.125,-4.3125 14.5625,-6.75 15,-6.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="273">
  <objectgroup draworder="index">
   <object id="1" x="13.25" y="8.625">
    <polygon points="0,0 -1.875,-2.5625 -6.125,-3.625 -13.1875,-7.625 -13.375,4.3125 -4.3125,4.1875"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="389">
  <objectgroup draworder="index">
   <object id="1" x="0.8125" y="8.125">
    <polygon points="0,0 6.25,-3.75 15.125,-5.1875 15.1875,4.9375 11.125,5.8125 3.375,4.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="390">
  <objectgroup draworder="index">
   <object id="1" x="0.1875" y="3">
    <polygon points="0,0 6.75,1.875 12.8125,4 12.75,7 8.8125,9.875 -0.1875,9.9375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="608">
  <objectgroup draworder="index">
   <object id="4" x="9.125" y="5.25" width="6.875" height="6.9375"/>
  </objectgroup>
 </tile>
 <tile id="609">
  <objectgroup draworder="index">
   <object id="1" x="-0.125" y="-0.0625" width="16.5625" height="16.6875"/>
  </objectgroup>
 </tile>
 <tile id="610">
  <objectgroup draworder="index">
   <object id="2" x="7.9375" y="0.0625">
    <polygon points="0.0625,-0.1875 0,4.625 7.9375,7 8,11.4375 0.5625,15.875 -7.875,15.9375 -8,-0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="611">
  <objectgroup draworder="index">
   <object id="1" x="0" y="6" width="3.8125" height="5.875"/>
  </objectgroup>
 </tile>
</tileset>
