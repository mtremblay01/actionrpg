<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="rogue maps" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../Art/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="Dirt" tile="-1"/>
 </terraintypes>
 <tile id="518" terrain="0,0,0,"/>
 <tile id="519" terrain="0,0,,0"/>
 <tile id="520" terrain=",,,0"/>
 <tile id="521" terrain=",,0,0"/>
 <tile id="522" terrain=",,0,"/>
 <tile id="529">
  <objectgroup draworder="index">
   <object id="1" x="0.0909091" y="10.6364" width="14.6364" height="5.36364"/>
  </objectgroup>
 </tile>
 <tile id="575" terrain="0,,0,0"/>
 <tile id="576" terrain=",0,0,0"/>
 <tile id="577" terrain=",0,,0"/>
 <tile id="578" terrain="0,0,0,0"/>
 <tile id="579" terrain="0,,0,"/>
 <tile id="634" terrain=",0,,"/>
 <tile id="635" terrain="0,0,,"/>
 <tile id="636" terrain="0,,,"/>
 <tile id="644">
  <objectgroup draworder="index">
   <object id="1" x="-0.0909091" y="3.90909" width="16.0909" height="12"/>
  </objectgroup>
 </tile>
 <tile id="650">
  <objectgroup draworder="index">
   <object id="1" x="4.54545" y="7.63636" width="6.27273" height="7.90909"/>
  </objectgroup>
 </tile>
 <tile id="1358">
  <objectgroup draworder="index">
   <object id="1" x="0.0909091" y="14.5455" width="15.8182" height="1.36364"/>
  </objectgroup>
 </tile>
 <tile id="1359">
  <objectgroup draworder="index">
   <object id="2" x="-0.0909091" y="14.3636" width="16" height="1.54545"/>
  </objectgroup>
 </tile>
 <tile id="1360">
  <objectgroup draworder="index">
   <object id="1" x="-0.0909091" y="14.7273" width="16" height="1.36364"/>
  </objectgroup>
 </tile>
 <tile id="1362">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0.272727" width="2.81818" height="15.5455"/>
  </objectgroup>
 </tile>
 <tile id="1363">
  <objectgroup draworder="index">
   <object id="1" x="0" y="14.8182" width="15.9091" height="1.18182"/>
  </objectgroup>
 </tile>
 <tile id="1489">
  <objectgroup draworder="index">
   <object id="3" x="0.0909091" y="0.0909091" width="15.8182" height="15.8182"/>
  </objectgroup>
 </tile>
</tileset>
