<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="collision box" tilewidth="16" tileheight="16" tilecount="1" columns="1">
 <image source="../Art/collision_box.png" width="16" height="16"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0.0909091" y="0" width="16" height="16.2727"/>
  </objectgroup>
 </tile>
</tileset>
