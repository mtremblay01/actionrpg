﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private Transform bar;

    // Start is called before the first frame update
    void Start()
    {
        bar = bar = transform.Find("Bar");
    }

    public void UpdateHealthBar(int healthPoints, int maxHealth)
    {
        bar.localScale = new Vector3((float)healthPoints / (float)maxHealth, bar.localScale.y, bar.localScale.z);
    }
}
