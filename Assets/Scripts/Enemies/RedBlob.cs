﻿using Assets.Scripts.Enemies;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBlob : EnemyBase
{
    // Start is called before the first frame update
    void Start()
    {
        base.Initialize(EnemyStatConfigs.RedBlob);
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerIsInSight)
        {
            ChasePlayer();
        }
    }
}
