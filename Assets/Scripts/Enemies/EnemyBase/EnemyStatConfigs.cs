﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Enemies
{
    public static class EnemyStatConfigs
    {
        public readonly static float TimeToForgetPlayer = 5f;
        public readonly static float ActionDuration = 1f;

        public static EnemyStats RedBlob
        {
            get
            {
                return new EnemyStats
                {
                    DamagePerHit = 10,
                    AttackSpeed = 10f,
                    RestBetweenAttacks = 1f,
                    HealthPoints = 100,
                    MoveSpeed = 3f,
                    ViewDistance = 5f,
                };
            }
        }
    }
}
