﻿using Tiled2Unity;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
    public abstract class EnemyBase : MonoBehaviour, IEnemy
    {
        protected EnemyStats _stats;

        public float BounceForce;
        public bool _isMoving;
        private Vector2 _lastDirection;
        private int _healthPoints;

        public GameObject BloodParticules;

        private float _lastActionChange;
        private float _lastEncounter;

        protected PlayerController _player;
        private Rigidbody2D _rigidbody;

        private SpriteDepthInMap _spriteDepthInMap;
        private EnemyAction _enemyAction;

        public void Initialize(EnemyStats stats)
        {
            _stats = stats;
            _healthPoints = _stats.HealthPoints;
            _lastActionChange = Time.realtimeSinceStartup;
            _player = FindObjectOfType<PlayerController>();
            
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void ChasePlayer(bool pushPlayer = false)
        {
            _enemyAction = EnemyAction.ChasingPlayer;
        
            if (!pushPlayer && IsTouchingPlayer)
            {
                return;
            }

            transform.position = Vector3.MoveTowards(transform.position, PlayerPosition, FixedMoveSpeed);
        }

        public void RunFromPlayer()
        {
            transform.position = Vector2.MoveTowards(transform.position, PlayerPosition, -FixedMoveSpeed);
        }

        public void MoveAround()
        {
            if (Time.realtimeSinceStartup - _lastActionChange > EnemyStatConfigs.ActionDuration)
            {
                _isMoving = !_isMoving;
                _lastActionChange = Time.realtimeSinceStartup;

                if (_isMoving)
                    _lastDirection = new Vector2(UnityEngine.Random.Range(-1, 2), UnityEngine.Random.Range(-1, 2));
                else
                    _lastDirection = Vector2.zero;
            }

            transform.Translate(_lastDirection * _stats.MoveSpeed * Time.deltaTime);
        }

        public void TakeDamage(Vector3 attackersPosition, int damage, float force)
        {
            Debug.Log($"{this.GetType().ToString()} : {_healthPoints}");
            _healthPoints -= damage;

            if (_healthPoints <= 0)
                Destroy(transform.gameObject);

            
            Vector3 direction = transform.position - attackersPosition;

            direction.Normalize();
            _rigidbody.AddForce(direction * force);

            Instantiate(BloodParticules, transform.position, Quaternion.identity);
        }

        public EnemyStats Stats
        {
            get
            {
                return _stats;
            }
        }

        protected float FixedMoveSpeed
        {
            get
            {
                return _stats.MoveSpeed * Time.deltaTime;
            }
        }

        public bool WasPlayerSeen {

            get
            {
                return Time.realtimeSinceStartup - _lastEncounter < EnemyStatConfigs.TimeToForgetPlayer;
            }
            private set { }
        }

        public bool PlayerIsInSight
        {
            get
            {
                return DistanceFromPlayer < _stats.ViewDistance;
            }
            private set { }
        }

        public float DistanceFromPlayer
        {
            get
            {
                return Vector3.Distance(transform.position, _player.transform.position);
            }
            private set { }
        }

        protected Vector3 PlayerPosition
        {
            get
            {
                return _player.transform.position;
            }
        }

        protected float LastActionChange
        {
            get
            {
                return _lastActionChange;
            }
        }

        protected float LastEncounter
        {
            get
            {
                return _lastEncounter;
            }
        }

        public Vector2 LastDirection
        {
            get {
                return _lastDirection;
            }
        }

        public bool IsTouchingPlayer
        {
            get
            {
                Collider2D collider = GetComponent<Collider2D>();
                return collider.IsTouching(_player.GetComponent<Collider2D>());
            }
        }

        public EnemyAction CurrentAction
        {
            get
            {
                return _enemyAction;
            }
        }

        public void OnCollisionStay2D(Collision2D other)
        {
            //Bump on world objects
            if (_enemyAction == EnemyAction.ChasingPlayer && other.gameObject.name != "Player")
            {
                Vector3 direction = new Vector3();

                if (other.collider.GetType() == typeof(PolygonCollider2D) || other.collider.GetType() == typeof(BoxCollider2D))
                {
                    //Polygon collider calculate its position from center bottom.
                    //Box collider not tested.
                    float colliderCenterX = other.collider.bounds.center.x;
                    float colliderCenterY = other.collider.bounds.center.y + other.collider.bounds.size.y / 2;

                    Vector3 colliderCenter = new Vector3(other.collider.bounds.center.x,
                                                         other.collider.bounds.center.y + other.collider.bounds.size.y / 2, other.collider.transform.position.z);

                    direction = transform.position - colliderCenter;                    
                }
                //Any round shapes
                else
                    direction = transform.position - other.transform.position;

                direction.Normalize();
                _rigidbody.AddForce(direction * BounceForce);
            }

            if(other.gameObject.name == "Player")
            {
                float rndDamage = Random.Range(_stats.DamagePerHit * 0.80f, (float)_stats.DamagePerHit);
                int damage = (int)Mathf.Floor(rndDamage);
                _player.LoseHealth(damage);
            }
        }
    }
}
