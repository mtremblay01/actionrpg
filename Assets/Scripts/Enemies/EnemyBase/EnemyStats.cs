﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Enemies
{
    public class EnemyStats
    {
        public int DamagePerHit { get; set; }
        public float AttackSpeed { get; set; }
        public int HealthPoints { get; set; }
        public float RestBetweenAttacks { get; set; }
        public float MoveSpeed { get; set; }
        public float ViewDistance { get; set; }
    }
}
