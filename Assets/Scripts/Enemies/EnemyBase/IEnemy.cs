﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
    public interface IEnemy
    {
        void Initialize(EnemyStats stats);
        void MoveAround();
        void TakeDamage(Vector3 attackersPosition, int damage, float force);
        void ChasePlayer(bool pushPlayer = false);
        void RunFromPlayer();
    }
}
