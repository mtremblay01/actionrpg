﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    private PlayerController player;
    private CameraController camera;

    public Vector2 playerOrientation;

    public bool isResapwnPoint;

    // Start is called before the first frame update
    void Start()
    {
        spawnPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if (isResapwnPoint && player.playerDead)
        {
            if(Time.realtimeSinceStartup - player.respawnDelay > player.timeOfDeath)
            {
                player.playerDead = false;
                player.gameObject.SetActive(true);
                spawnPlayer();
            }
        }
    }

    void spawnPlayer()
    {
        player = FindObjectOfType<PlayerController>();
        camera = FindObjectOfType<CameraController>();

        player.transform.position = transform.position;
        camera.transform.position = new Vector3(transform.position.x, transform.position.y, camera.transform.position.z);

        player.lastMovement = playerOrientation;
    }
}
