﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static bool CameraExists;
    public GameObject target;
    public float moveSpeed;
    public float movementTreshhold;
    public float distance;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, this.transform.position.z); ;

        DontDestroyOnLoad(transform.gameObject);

        if (!CameraExists)
        {
            CameraExists = true;
        }
        else
        {
            Destroy(transform.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, this.transform.position.z);

        distance = Vector3.Distance(transform.position, targetPosition);

        if ( distance > movementTreshhold)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, moveSpeed * Time.deltaTime);
        }
    }
}
