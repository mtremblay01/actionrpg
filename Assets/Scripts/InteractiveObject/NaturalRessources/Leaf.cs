﻿using System.Collections;
using System.Collections.Generic;
using Tiled2Unity;
using UnityEngine;

public class Leaf : MonoBehaviour
{
    private Animator _animator;
    private SpriteDepthInMap _spriteDepthInMap;

    // Start is called before the first frame update
    void Start()
    {
        _spriteDepthInMap = GetComponent<SpriteDepthInMap>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _spriteDepthInMap.UpdateSpriteDepth();

        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Over"))
        {
            Destroy(gameObject);
        }
    }
}
