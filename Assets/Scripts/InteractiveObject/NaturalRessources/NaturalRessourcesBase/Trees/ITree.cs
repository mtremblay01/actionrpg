﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.InteractiveObject.NaturalRessources.NaturalRessourcesBase.Trees
{
    public interface ITree
    {
        void LoseToughness(int damage);
    }
}
