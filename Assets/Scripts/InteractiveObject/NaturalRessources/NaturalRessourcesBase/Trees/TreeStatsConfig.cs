﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.InteractiveObject.NaturalRessources.NaturalRessourcesBase.Trees
{
    public static class TreeStatsConfig
    {
        public static TreeStats Oak
        {
            get
            {
                return new TreeStats
                {
                    Toughness = 50
                };
            }
        }
    }
}
