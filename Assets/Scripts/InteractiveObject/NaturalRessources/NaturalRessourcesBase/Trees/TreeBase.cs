﻿using Assets.Scripts.InteractiveObject.NaturalRessources.NaturalRessourcesBase.Trees;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TreeBase : MonoBehaviour, ITree
{
    private PolygonCollider2D _collider;
    private TreeStats _stats;
    private int _toughnessLeft;

    public GameObject CutDown;
    public GameObject GrownUp;
    public GameObject BarkParticules;
    public GameObject Leaf;

    public void Initialize(TreeStats stats)
    {
        _stats = stats;
        _toughnessLeft = _stats.Toughness;
    }

    public void LoseToughness(int damage)
    {
        _toughnessLeft -= damage;
        
        if (_toughnessLeft <= 0)
            GrownUp.SetActive(false);

        if (BarkParticules)
            Instantiate(BarkParticules, transform.position, Quaternion.identity);

        if (Leaf)
            InstantiateLeafs();

        // Log //
        Debug.Log(this.GetType().ToString() + " : " + _toughnessLeft);
    }

    private void InstantiateLeafs()
    {
        for(int i = 0; i < 3; i++)
        {
            var leaf = Instantiate(Leaf, transform, true);
            leaf.transform.position = new Vector3(Random.insideUnitCircle.x, Random.insideUnitCircle.y, transform.position.z);
            leaf.transform.position = new Vector3(transform.position.x, transform.position.y) + leaf.transform.position;
        }
    }
}
