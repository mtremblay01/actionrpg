﻿using Assets.Scripts.InteractiveObject.NaturalRessources.NaturalRessourcesBase.Trees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.InteractiveObject.NaturalRessources
{
    public class Oak : TreeBase
    {
        void Start()
        {
            Initialize(TreeStatsConfig.Oak);
        }
    }
}
