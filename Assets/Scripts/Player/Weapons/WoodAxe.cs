﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.InteractiveObject.NaturalRessources.NaturalRessourcesBase.Trees;

namespace Assets.Scripts.Weapons
{
    class WoodAxe : WeaponBase
    {
        void Start()
        {
            base.Initialize(WeaponStatConfigs.WoodAxe);
        }

        public override void Attack()
        {
            HurtEnemies();
            ChopWood();
        }

        private void ChopWood()
        {
            foreach(RaycastHit2D cast in GetCapsuleCasts())
            {
                if(cast && cast.transform.gameObject.tag == "Tree")
                {
                    ITree tree = cast.transform.gameObject.GetComponent<ITree>();
                    tree.LoseToughness(_stats.DamagePerHit);
                }
            }
        }
    }
}
