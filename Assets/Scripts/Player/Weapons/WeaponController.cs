﻿using Assets.Scripts.Weapons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private PlayerController _player;
    public List<GameObject> WeaponList;
    public IWeapon ActiveWeapon;
    public GameObject ActiveWeaponObject;

    public bool IsAttacking { get; set; }
    private Animator _playerAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<PlayerController>();
        IsAttacking = false;
        _playerAnimator = _player.PlayerAnimator;
        ActiveWeapon = ActiveWeaponObject.GetComponent<IWeapon>();
        ActiveWeaponObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePositionAndRotation();
        ListenForInput();
    }

    private void UpdatePositionAndRotation()
    {
        Vector3 playerPostion = _player.transform.position;
        transform.position = playerPostion;
        transform.rotation = _player.transform.rotation;
        transform.Translate(_player.lastMovement);

        if ( ActiveWeaponObject.activeSelf )
        {
            ActiveWeapon.FixRotation();
        }
    }

    private void ListenForInput()
    {
        if ( Input.GetButton("Fire1") && !IsAttackAnimationPlaying )
        {
            _playerAnimator.SetBool("IsAttacking", true);
        }
        else
        {
            _playerAnimator.SetBool("IsAttacking", false);
            ActiveWeaponObject.SetActive(false);
        }        
        _player.CanPlayerMove = !_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Attack");
    }

    public void Attack()
    {
        ActiveWeaponObject.SetActive(true);
    }

    private void BlockPlayerMovement()
    {
        if (_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            _player.CanPlayerMove = false;
        }
        else
        {
            _player.CanPlayerMove = true;
        }
        
    }

    private bool IsAttackAnimationPlaying
    {
        get
        {
            return _playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Attack");
        }
    }
}
