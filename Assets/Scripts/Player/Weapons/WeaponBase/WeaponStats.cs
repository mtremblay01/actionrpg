﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Weapons
{
    public class WeaponStats
    {
        public int DamagePerHit { get; set; }
        public float PushForce { get; set; }
    }
}
