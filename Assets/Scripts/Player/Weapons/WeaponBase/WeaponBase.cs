﻿using Assets.Scripts.Enemies;
using Assets.Scripts.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

using Random = UnityEngine.Random;

namespace Assets.Scripts.Weapons
{
    public abstract class WeaponBase : MonoBehaviour, IWeapon
    {
        public WeaponStats _stats { get; private set; }
        private CapsuleCollider2D _collider;
        private PlayerController _player;
        

        public void Initialize(WeaponStats stats)
        {
            _stats = stats;
            _collider = GetComponent<CapsuleCollider2D>();
            transform.gameObject.SetActive(false);
            _player = FindObjectOfType<PlayerController>();
        }

        void OnEnable()
        {
            attack();
        }

        private void attack()
        {
            if (!_collider)
                return;

            Attack();
        }
        public abstract void Attack();

        public void HurtEnemies()
        {  
            foreach (RaycastHit2D cast in GetCapsuleCasts())
            {
                if (cast && cast.transform.gameObject.tag == "Enemy")
                {
                    float rndDamage = Random.Range(_stats.DamagePerHit * 0.80f, (float)_stats.DamagePerHit);
                    int damage = (int)Mathf.Floor(rndDamage);
                    IEnemy enemy = cast.transform.gameObject.GetComponent<IEnemy>();
                    enemy.TakeDamage(_player.transform.position, damage, _stats.PushForce);
                }
            }
        }
        public void FixRotation()
        {
            transform.rotation = Quaternion.LookRotation(_player.lastMovement);            
        }

        protected RaycastHit2D[] GetCapsuleCasts()
        {
            return Physics2D.CapsuleCastAll(_collider.transform.position, _collider.size, _collider.direction, 0f, Vector2.zero);            
        }
    }
}
