﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Weapons
{
    public interface IWeapon
    {
        void Attack();
        void FixRotation();
    }
}

