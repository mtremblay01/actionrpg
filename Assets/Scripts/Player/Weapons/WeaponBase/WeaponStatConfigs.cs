﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Weapons
{
    public static class WeaponStatConfigs
    {
        public static WeaponStats WoodAxe
        {
            get
            {
                return new WeaponStats
                {
                    DamagePerHit = 10,
                    PushForce = 400
                };
            }
        }
    }
}
