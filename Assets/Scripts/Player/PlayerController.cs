﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private static bool PlayerExists;
    private Rigidbody2D myRigidbody;
    public WeaponController WeaponController;

    public float movementSpeed;
    public float respawnDelay;
    public int maxHealth;
    public int healthPoints;
    public float recoverDelay;
    public float force;

    public Vector2 lastMovement;
    private bool playerMoving;
    public float timeOfDeath;
    public bool playerDead;
    private float lastHit;

    public bool CanPlayerMove { get; set; }

    public bool IsMoving { get; internal set; }


    // Start is called before the first frame update
    void Start()
    {
        this.myRigidbody = GetComponent<Rigidbody2D>();
        PlayerAnimator = GetComponent<Animator>();

        CanPlayerMove = true;
        DontDestroyOnLoad(transform.gameObject);
        
        if (!PlayerExists)
        {
            PlayerExists = true;
        }
        else
        {
            Destroy(transform.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {        
        move();
    }

    private void move()
    {
        if (!CanPlayerMove)
        {
            IsMoving = false;
            this.myRigidbody.velocity = new Vector2(0f, 0f);
            return;
        }

        var horizontalInput = Input.GetAxisRaw("Horizontal");
        var verticalInput = Input.GetAxisRaw("Vertical");
        this.playerMoving = false;

        if (horizontalInput > 0.5f || horizontalInput < -0.5f)
        {
            IsMoving = true;

            this.lastMovement = new Vector2(horizontalInput, 0f);
            this.playerMoving = true;

            float speed = horizontalInput * movementSpeed;
            this.myRigidbody.velocity = new Vector2(speed, myRigidbody.velocity.y);
        }
        else{
            IsMoving = false;

            this.myRigidbody.velocity = new Vector2(0f, myRigidbody.velocity.y);
        }

        if (verticalInput > 0.5f || verticalInput < -0.5f)
        {
            IsMoving = true;
            this.lastMovement = new Vector2(0f, verticalInput);
            this.playerMoving = true;
            
            float speed = verticalInput * movementSpeed;
            this.myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, speed);
        }
        else{
            IsMoving = false;
            this.myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, 0f);
        }

        animateMovement();
    }
    
    private void animateMovement(){
        var horizontalInput = Input.GetAxisRaw("Horizontal");
        var verticalInput = Input.GetAxisRaw("Vertical");

        this.PlayerAnimator.SetFloat("MoveX", horizontalInput);
        this.PlayerAnimator.SetFloat("MoveY", verticalInput);

        this.PlayerAnimator.SetBool("PlayerMoving", this.playerMoving);
        this.PlayerAnimator.SetFloat("LastMoveX", this.lastMovement.x);
        this.PlayerAnimator.SetFloat("LastMoveY", this.lastMovement.y);
    }

    public void LoseHealth(int damage)
    {
        bool recoverTimeOver = Time.realtimeSinceStartup - lastHit > recoverDelay;
        if (recoverTimeOver)
        {
            lastHit = Time.realtimeSinceStartup;
            healthPoints -= damage;
            Debug.Log("Player : " + healthPoints);
            if (healthPoints <= 0)
                respawn();
        }
    }

    public void respawn()
    {
        playerDead = true;
        healthPoints = maxHealth;
        timeOfDeath = Time.realtimeSinceStartup;
        transform.gameObject.SetActive(false);
    }

    public Animator PlayerAnimator { get; private set; }

    public void Attack()
    {
        WeaponController.Attack();
    }
}
